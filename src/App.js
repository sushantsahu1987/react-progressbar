import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import Progressbar from "./Progressbar";

class App extends Component {
  render() {
    return (
      <div className="App">
        <Progressbar percentage={30}/>
      </div>
    );
  }
}

export default App;
