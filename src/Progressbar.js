import React,{Component} from 'react'

class Progressbar extends Component {

    constructor(props){
        super(props)

        this.state = { width : '100px'}

        this.changeValue = this.changeValue.bind(this)

        {
            // setTimeout(this.changeValue,2000)
            this.id = setInterval(this.changeValue,2000)
        }

    }


    render(){
        return (
            <div>
                Hello World {this.props.percentage}
                {
                    this.w = this.state.width
                }


                <div style={styles.container}>
                    <div style={{width: this.w, height: '25px', backgroundColor:'#00ff00'}}>
                    </div>
                </div>
            </div>
        )
    }

    changeValue() {
        console.log('change value')
        const rand = Math.floor(Math.random() * 499);
        console.log('random no. : '+rand)
        let v = rand+'px'

        if(rand < 100) {
            clearInterval(this.id)
        }else {
            this.setState({
                width: v
            })
        }

    }

}

const styles = {

    container: {
        marginLeft:'10%',
        marginTop:'10%',
        width: '500px',
        height: '25px',
        backgroundColor:'#ff0000',
    },

    incontainer: {
        height: '25px',
        backgroundColor:'#00ff00',
        transitionProperty: 'width 2s',
        transitionDuration:'4s'
    }



}

export default Progressbar